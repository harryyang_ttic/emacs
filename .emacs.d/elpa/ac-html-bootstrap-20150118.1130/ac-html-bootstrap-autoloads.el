;;; ac-html-bootstrap-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (ac-html-bootstrap+) "ac-html-bootstrap" "ac-html-bootstrap.el"
;;;;;;  (21697 22062 36743 158000))
;;; Generated autoloads from ac-html-bootstrap.el

(autoload 'ac-html-bootstrap+ "ac-html-bootstrap" "\
Enable bootstrap ac-html completion

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("ac-html-bootstrap-pkg.el") (21697 22062
;;;;;;  76583 924000))

;;;***

(provide 'ac-html-bootstrap-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ac-html-bootstrap-autoloads.el ends here
